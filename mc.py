#Our Project 1 starter program for calculating pi
import numpy
import matplotlib.pyplot as plt

#We want to calculate pi using the Monte Carlo Method
#Throw darts at a square dartboard

#Count darts that go in circle vs total number of darts = pi/4
#Need ability to throw darts at a square
#Empty def throw_dart code
def throw_dart():
	x = numpy.random.random() #each of these should be between 0 and 1
	y = numpy.random.random()
	return x,y
	
#Need ability to determine if the dart is in the circle
def in_circle(point): 
	d = (point[0]**2 + point[1]**2)**(0.5)
	if d > 1.0:
		return False
	return True
#Need the ability to determine the number of darts inside the circle vs outside the circle.
n=1
#Created the loop marker
numTrue = 0
numFalse = 0
#Created variables that store number of true and false instances. 

#Added while loop that counts number of true and false instances in 10000 attempts


PiList =[]
nList =[]
while (n<10000000):
	n=n+1
	if in_circle(throw_dart()):
		numTrue +=1
	else:
		numFalse +=1
	Pi=(numTrue/n)*4
#Added a list	
	PiList.append(Pi)
#Commands computer to put n into the list
	nList.append(n)


#Allowed us to use plt to create plot
Pi=(numTrue/n)*4
print(numTrue)
print(numFalse)
print(Pi)
plt.plot(nList, PiList)
plt.xlabel('Trials')
plt.ylabel('Pi Value')
plt.ylim(3.1,3.2)
plt.show()
