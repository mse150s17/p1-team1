# README #

This README will be for Team 1's project 1 repository for calculating pi.
This code allows you to calculate pi using the Monte Carlo method.

### What is this repository for? ###

* Quick summary: The purpose of this repository is to create an overview of our project, with descriptions of the codes we used, why we used them, and also to keep track of any previous edits that have been done while working on the project.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo): Markdown is a text-to-HTML conversion tool that is used by computer programmers to simply write HTML code. It is a software that turns a plain text into a valid HTML. This makes programming less complicated for the average user by making Markdown's format as easy as possible to read. A Markdown-formatted file should be publishable, as written, with no formatting editing required.

### How do I get set up? ###

* Summary of set up: In order to set up a repository, a new directory must first be created. Next, open the directory and type "git init" and hit enter. After that, the desired code can then be written and saved as a file, which is given a filename. Type "git add 'filename'" and hit enter. Then type "git commit" and hit enter (option to add a comment will appear). The first file in a repository is usually a README or Markdown describing the purpose of the directory or project.
* Configuration: This is used to customize a git environment on a single computer that does not change until it is editted. The tool that does this is called "git config" and it installs and sets the different configuration variables that are needed to use git. Configuration gives a user a global identity by typing and entering "git config --global user.name "enter name"" and "git config --global user.email "enter email"." Now commits can be attributed to the user. The last setting to choose is the default text editor application. Finally, the computer has been congfigured, and the settings will remain, even through updates or upgrades.
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
* Write comments for the code in the repository
* Make functions to perform useful tasks
* Write tests for portions of the code
* Provide constructive feedback to your peers about their changes
* Identify issues and post them on bitbucket
* Resolve issues posted to bitbucket

### Who do I talk to? ###

* Repo owner or admin: An advantage an owner has over an admin is the ability to push, pull, and clone in all of the repositories in an organization. If there is something wrong with the repository, an owner would be best to contact.
* Other community or team contact